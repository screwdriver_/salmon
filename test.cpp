#include <iostream>
#include <vector>

#include <regression/regression.hpp>
#include <salmon.hpp>

using namespace std;

int main(void)
{
	using namespace salmon;

	dataset<float> d(1);
	d.push(1, 1);
	std::cout << ordinary_least_square(d.regressor_matrix(), d.get_regressand()) << std::endl;
	return 0;
}
