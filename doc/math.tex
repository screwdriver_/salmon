\documentclass{report}
\usepackage{mathtools}
\usepackage{amssymb}

\title{Salmon - Mathematical overview}
\author{Luc Len\^{o}tre}

\begin{document}
	\maketitle
	\newpage

	\tableofcontents{}

	\newpage

	\chapter{Introduction}

	Salmon is a an open source general puropose C++ machine learning library.
	This document is meant to, first and foremost, describe the inner mathematics behind the library and to give the references to the models used in it.
	This document might change in the future as new features are added to the library.

	The same statistical model cannot apply to any kind of problem, regarding the nature of the input data and the information that we want to extract from it. This is why a model should be built according to the problem it has to solve. This library provides a set of tools that can be assembled, to produce a specific model for the situation at has been built for.



	\section{Definitions}

	This sections contains definitions for terms used in this document.

	\begin{itemize}
		\item sample: An observation of the phenomenon that is to be studied.
		\item dependent variable: The output variable of a statistical model.
		\item independent variable: A variable containing a value (a sample) that is used as input for a statistical model.
		\item odds: The likelihood of an event to take place, defined as $p / (1 - p)$.
		\item homoscedasticity: A variable set is said to be homoscedastic if all the variables have the same, finite variance (The opposite is called "heteroscedasticity").
		\item filter: A process allowing to remove outliers from a set of samples.
		\item outlier: A value that is too far from other samples to be considered as a correct value.
		\item model: A set of statistical tools (such as filters, estimators, etc...) applied in a pipeline-fashon on the input data. A model is constructed in a specific way for the kind of problems it has to solve.
		\item model fitting: The process of finding the best constants for a given model regarding the given samples.
		\item estimator: A statistical tool which allows to make estimations for values based on model fitting made with sample data.
		\item breakdown point: A property of an estimator that corresponds to the minimal amount of incorrect data it has to get as inputs to give incorrect results.
		\item generalized linear model (GLM): A generalization of ordinary linear regression that allows the output variables to have an error distribution other than normal distribution.
		\item mean function: A function $\mu$ representing the mean of the distribution
		\item linear predictor: A relation of the form $\eta = \beta^T X$. Vector $\beta$ contains unobserved variables.
		\item link function: A function $g(\mu)$ providing the relationship between the linear predictor and the mean of the distribution for the model.
	\end{itemize}



	\section{Document conventions}

	In this document, indexes begin at 0 instead of 1 in order to stay unified with indexes on physical computers.



	\chapter{Regressions}

	\section{Linear regression}

	Linear regressions are statistical models allowing to establish a linear relationship between dependent and independent variables.
	The regression process returns constants for linear functions that are meant to provide an approximate representation of the dataset.

	According to the \textbf{Gauss-Markov theorem}, the best estimator for linear regressions is the \textbf{Ordinary Least Square (OLS)} estimator.



	\subsection{Simple linear regression}

	The formula for the function of the simple linear regression is the following:
	\begin{equation}
		y = \alpha + \beta x
	\end{equation}

	Where,
	\begin{itemize}
		\item $\alpha$ is the y-intercept
		\item $\beta$ is the slope
	\end{itemize}

	Constants $\alpha$ and $\beta$ both depend on the samples and the selected estimator.



	\subsection{Polynomial linear regression}

	This regression is a special case of linear regression. It allows the use of higher degrees by using a Vandermonde matrix during model fitting.

	Any kind of polynomial can be used for this estimator, so the general formula is:
	\begin{equation}
		y = \sum_{i=0}^{N} \alpha_i x^i
	\end{equation}

	Where,
	\begin{itemize}
		\item $N$ is the degree of the polynomial
		\item $\alpha$ is a $N$-vector of constants
	\end{itemize}



	\section{Logistic regression}

	The logistic regression is a \textbf{Generalized Linear Model (GLM)} based on a Bernoulli distribution.
	It is based upon the logistic function, whose formula is the following:
	\begin{equation}
		y = \frac{M}{1 + e^{-k(x - x_0)}}
	\end{equation}

	Where,
	\begin{itemize}
		\item $M$ is the maximum of the function
		\item $k$ is the growth rate
		\item $x_0$ is the $x$ position of the mid-point of the curve
	\end{itemize}

	We will be using values $M = 1, k = 1, x_0 = 0$:

	\begin{equation}
		\mu(x) = \sigma(x) = \frac{1}{1 + e^{-x}}
	\end{equation}

	Its derivative is:

	\begin{equation}
		\frac{\mathrm d}{\mathrm d x} \mu(x) = \sigma(x) (1 - \sigma(x))
	\end{equation}

	Let's explain why using the logistic function for probabilistic classification problems.
	We assume a relationship between the log-odds (logit function) $g(\mu)$ and the linear predictor $\eta$:

	\begin{equation}
	\begin{split}
		& g(\mu) = ln(\frac{\mu}{1 - \mu}) = \eta \\
		& \frac{\mu}{1 - \mu} = e^\eta \\
		& \mu = \frac{e^{\eta}}{1 + e^{\eta}} = \frac{1}{1 + e^{-\eta}} = \frac{1}{1 + e^{-(\beta^T X)}}
	\end{split}
	\end{equation}

	As the logistic regression model is supposed to classify data according to its probability, we can consider the formula to be an equivalent of the probability for the output to equal $1$.

	\begin{equation}
		h_{\beta}(X) = \mu(X) = \frac{1}{1 + e^{-(\beta^T X)}} = P(Y = 1 | X; \beta)
	\end{equation}

	$h_{\beta}(X)$ is the hypothesis function.

	Where,
	\begin{itemize}
		\item $\beta$ is a vector of constants
		\item $X$ is the sample data vector on the x axis
	\end{itemize}

	Then we can deduce that:

	\begin{equation}
		P(Y = 0 | X; \beta) = 1 - h_{\beta}(X)
	\end{equation}

	Since $Y \in \{0, 1\}$, the probabilty follows a Bernoulli distribution:

	\begin{equation}
		P(Y | X; \beta) = h_{\beta}(X)^{y} (1 - h_{\beta}(X))^{1 - y}
	\end{equation}

	Then, the likelihood function is the following:

	\begin{equation}
	\begin{split}
		L(\beta | X) & = P(Y | X; \beta) \\
			& = \prod_{i=0}^{m} P(y_{i} | x_{i}; \beta) \\
			& = \prod_{i=0}^{m} h_{\beta}(x_{i})^{y_{i}} (1 - h_{\beta}(x_{i}))^{1 - y_{i}}
	\end{split}
	\end{equation}

	Consider the log-likelihood:

	\begin{equation}
	\begin{split}
		log(L(\beta | X)) & = log(\prod_{i=0} h_{\beta}(x_{i})^{y_{i}} (1 - h_{\beta}(x_{i}))^{1 - y_{i}}) \\
			& = \sum_{i=0}^{m} log(h_{\beta}(x_{i})^{y_{i}} (1 - h_{\beta}(x_{i}))^{1 - y_{i}}) \\
			& = \sum_{i=0}^{m} y_{i} log(h_{\beta}(x_{i})) + (1 - y_{i}) log(1 - h_{\beta}(x_{i}))
	\end{split}
	\end{equation}

	Maximizing the log-likelihood allows to determine the constants $\beta$ that best fit the model for the given independent variables $X$.
	Thus, the cost function is:

	\begin{equation}
	\begin{split}
		J(\beta) & = -\frac{1}{m} log(L(\beta | X)) \\
				 & = -\frac{1}{m} \sum_{i=0}^{m} y_{i} log(h_{\beta_{i}}(x_{i})) + (1 - y_{i}) log(1 - h_{\beta_{i}}(x_{i}))
	\end{split}
	\end{equation}

	And its derivative is:

	\begin{equation}
		\frac{\mathrm d}{\mathrm d \beta_j} J(\beta) = \frac{1}{m} \sum_{i=0}^{m} (h_{\beta}(x_{i}) - y_{i}) x_{i}
	\end{equation}

	The cost function can be minimized using the \textbf{Ordinary Least Square} (OLS) method.



	\chapter{Estimators}

	\section{Ordinary Least Square (OLS)}

	Consider the following hypothesis function:
	\begin{equation}
		y_{i} = h_{\beta}(x) = \beta_{i} x_{i} + \epsilon_{i}
	\end{equation}

	Where,
	\begin{itemize}
		\item $\beta$ is a vector of constants
		\item $x$ is a vector of x coordinates
		\item $y$ is a vector of y coordinates
		\item $\epsilon$ is a vector of unobservable error variables
	\end{itemize}

	The goal is to find the constant $\beta$ such that the equation fits the best.
	The cost function $J(\beta)$ is the \textbf{Mean Squared Error} (MSE), defined as follows:

	\begin{equation}
		J(\beta) = \frac{1}{m} \sum_{i=0}^{m} ( y_m - h_{\beta_{i}}(x_i) )^2
	\end{equation}

	$J(\beta)$ represents the squared difference between the first equation and the input data according to the given $\beta$ constants.
	Thus, minimizing this function $J(\beta)$ gives the constants $\beta$ such as the model is the closest to the sample values.

	The solution for this minimization problem uses a \textbf{Moore-Penrose inverse}:
	\begin{equation}
	\begin{split}
		\hat{\beta} & = min J(\beta) \\
					& = (x^T x)^{-1} x^T y
	\end{split}
	\end{equation}



	\subsection{Using Vandermonde matrix}

	A Vandermonde matrix can be used instead, making a polynomial regression.
	The amount of lines corresponds to the number of samples and the amount of columns corresponds to the degree of the polynom.

	The Vandermonde matrix is defined as follows:
	\begin{equation}
		V_{ij} = x_{i}^j
	\end{equation}



	\section{Theil-Sen estimator}

	TODO



	\section{Gradient Descent (GD)}

	The gradient descent algorithm allows to find a local minimum for a given convex function.
	The general idea is to start the search from a given point and to go against the gradient of the function to reach a minimum.

	Let $f(x)$ be a differentiable function.
	Let $x_0$ be the beginning value of the algorithm.
	Let $\gamma \in \mathbb{R}_{+}$ be the stepping constant.

	\begin{equation}
		x_{n + 1} = x_n - \gamma \nabla f(x_n)
	\end{equation}

	The more iterations are performed, the closer the algorithm gets from the closest minimum.

	This method has two main inconvenients:
	\begin{itemize}
		\item The found minimum might not be the global minimum but only a local minimum
		\item The optimization might be zigzagging slowly if passing in a narrow valley (like for the Rosenbrock function)
	\end{itemize}



	\subsection{Gradient Ascent (GA)}

	The same method as the \textbf{Gradient Descent} can be used to find the local maximum of a function.
	The formula has to be replaced by the following one:

	\begin{equation}
		x_{n + 1} = x_n + \gamma \nabla f(x_n)
	\end{equation}

	The only difference is that the algorithm uses a $+$ instead of $-$ in order to move toward the maximum.



	\subsection{Stochastic Gradient Descent (SGD)}

	TODO



	\chapter{Preprocessing methods}

	\section{Data augmentation}

	TODO



	\section{Outliers filtering}

	\subsection{Mahalanobis distance}

	TODO



	\chapter{Neural networks}

	\textbf{Artificial Neural Networks} (ANN) are the basis of Machine Learning.
	This chapter desribes different types of ANN.



	\section{Perceptron}

	A perceptron is a fundamental part of a basic neural network.
	It is a binary classifier algorithm, which means that is takes inputs and decides whether it is part of a category or not.

	Here is the general formula for a perceptron:

	\begin{equation}
		y = f(\sum_{i=0}^{m} w_{i} x_{i})
	\end{equation}

	Where,
	\begin{itemize}
		\item $f$ is the activation function
		\item $m$ is the number of inputs
		\item $w$ is the weights vector
		\item $x$ is the inputs vector
		\item $y$ is the output value
	\end{itemize}

	Each inputs is linked to a weight, determining the impact of each inputs on the final result.

	There exist several activation functions (not exhaustive):
	\begin{itemize}
		\item Sigmoid function: S-shaped curve
		\item Threshold function: A function returning 1 if the input value is higher than the specified threshold
	\end{itemize}



	\section{Multilayer perceptron}

	A \textbf{MultiLayer Perceptron} (MLP) is a basic neural network.
	The network is a succession of layers of perceptrons. Each layer must have at least one node.

	There are three types of layers:
	\begin{itemize}
		\item Input layer: The first layer. It has one node for each feature
		\item Hidden layer: There can be any number of hidden layers
		\item Output layer: The last layer. It has one node for each output values
	\end{itemize}

	MLPs are feedfoward networks, meaning that the relation between a layer and the next layer forms a complete bipartite graph. Data is treated in a pipeline fashion.

	Let's take the formula for a perceptron:

	\begin{equation}
		y = f(\sum_{i=0}^{m} w_{j,i} x_{i})
	\end{equation}

	And extend it to a multilayer perceptron:

	\begin{equation}
		y_{j}^{l} = f^{l}(\sum_{i=0}^{m} w_{i}^{l} y_{i}^{(l - 1)})
	\end{equation}

	Where,
	\begin{itemize}
		\item $l$ is the layer number
		\item $f^{l}$ is the activation function
		\item $m$ is the number of nodes on the previous layer
		\item $w_{j,i}^{l}$ is the weight on node $j$ for the $i$-th input in layer $l$
		\item $y_{i}^{l}$ is the output value of node $i$ in layer $l$
	\end{itemize}

	This formula allows to compute values for every nodes in the NN.

	Since we don't know the appropriate weights for the network, we need to train it from a set of data.
	A common method for training MLPs is \textbf{Backpropagation}.



	\chapter{Learning methods}

	\section{Learning paradigms}

	TODO



	\section{Backpropagation}

	Backpropagation is a very common \textbf{supervised learning} algorithm for \textbf{feedforward} NNs.

	TODO



	\chapter{Measure of accuracy}

	\section{Confusion matrix}

	The confusion matrix allows to visualize the accuracy of a classification model.
	Let $n$ be the number of classes for the model.

	The confusion matrix has size $n * n$.
	For the confusion matrix $C$, $C_{ij}$ represents the number of elements of the class $j$ that were classified as elements of the class $i$.

	Elements that have been correctly classified are placed on the diagonal of the matrix.

	The confusion matrix allows the computation of the \textbf{Matthews Correlation Coeficient} (MCC).

	TODO: Explain more derivations from the confusion matrix



	\subsection{Matthews Correlation Coeficient}

	Let $C$ be a $K * K$ confusion matrix.

	The associated MCC is defined as follows:

	\begin{equation}
		\text{MCC} = \frac{\sum_{k}\sum_{l}\sum_{m} C_{kk}C_{lm} - C_{kl}C_{mk}}{\sqrt{\sum_{k}(\sum_{l} C_{kl})(\sum_{k' | k' \neq k}\sum_{l'} C_{k'l'})} \sqrt{\sum_{k}(\sum_{l} C_{lk})(\sum_{k' | k' \neq k}\sum_{l'} C_{l'k'})}}
	\end{equation}

\end{document}
