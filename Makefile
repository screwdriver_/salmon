NAME = salmon.a
AR = ar
ARFLAGS = rc
CC = g++
CFLAGS = -Wall -Wextra -Werror -Wno-unused-result -std=c++17 -I src/ -g3

SRC_DIR = src/
SRC := $(shell find $(SRC_DIR) -type f -name "*.cpp")
HDR := $(shell find $(SRC_DIR) -type f -name "*.hpp")
DIRS := $(shell find $(SRC_DIR) -type d)
OBJ_DIR = obj/
OBJ := $(patsubst $(SRC_DIR)%.cpp,$(OBJ_DIR)%.o,$(SRC))
OBJ_DIRS := $(patsubst $(SRC_DIR)%,$(OBJ_DIR)%,$(DIRS))

LIBS = mathlib.a
INCLUDES = -I mathlib/src/

all: $(NAME) tags

$(OBJ_DIRS):
	mkdir -p $(OBJ_DIRS)

mathlib.a:
	make -C mathlib/ $@
	cp mathlib/mathlib.a mathlib.a

$(NAME): $(OBJ_DIRS) $(OBJ) $(LIBS)
	$(AR) $(ARFLAGS) $@ $(OBJ)

$(OBJ_DIR)%.o: $(SRC_DIR)%.cpp $(HDR)
	$(CC) $(CFLAGS) -o $@ -c $< $(INCLUDES)

tags: $(SRC) $(HDR)
	ctags $(SRC) $(HDR)

clean:
	make -C mathlib/ clean
	rm -r $(OBJ_DIR)

fclean: clean
	make -C mathlib/ fclean
	rm $(LIBS)
	rm $(NAME)
	rm tags

re: fclean all

.PHONY: all clean fclean re
