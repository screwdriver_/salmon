#ifndef IMAGE_HPP
# define IMAGE_HPP

# include <string>
# include <vector>

# include <salmon.hpp>

namespace salmon
{
	enum image_format
	{
		RAW,
		PNG,
		JPG,
		BMP
	};

	class image
	{
		public:
		image(size_t width, size_t height, size_t channels);

		inline size_t get_width() const
		{
			return width;
		}

		inline size_t get_height() const
		{
			return height;
		}

		inline size_t get_channels() const
		{
			return channels;
		}

		image subimage(size_t x, size_t y, size_t width, size_t height) const;

		void transform(const math::mat<float> &matrix);
		void transform(const math::mat<float> &matrix, const math::vec<float> &origin);
		void apply_kernel(const math::mat<float> &kernel);

		void crop();
		void expand();

		inline auto &get_data() { return data; }
		inline const auto &get_data() const { return data; }

		float *get(size_t x, size_t y);
		const float *get(size_t x, size_t y) const;

		dataset<float> to_dataset();

		static image read(const std::string &file);
		static void write(const std::string &file, image_format format);

		private:
		size_t width, height, channels;
		std::vector<float> data;
	};

	std::string read_file(const std::string &file);
	void write_file(const std::string &file, const std::string &data);
}

#endif
