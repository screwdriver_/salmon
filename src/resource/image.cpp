#include <resource/image.hpp>

#define BUFFER_SIZE(width, height, channels)	((width) * (height) * (channels))
#define ENTRY_OFFSET(x, y)						((x) * height + (y))

using namespace salmon;

image::image(size_t width, size_t height, size_t channels)
	: width{width}, height{height}, channels{channels}, data(BUFFER_SIZE(width, height, channels))
{}

image image::subimage(size_t x, size_t y, size_t width, size_t height) const
{
	image img(width, height, channels);

	for(size_t i(0); i < height; ++i)
		for(size_t j(0); j < width; ++j)
			*img.get(j, i) = *get(x + j, y + i);
	return img;
}

void image::transform(const math::mat<float> &matrix)
{
	std::vector<float> tmp(BUFFER_SIZE(width, height, channels));

	for(size_t i(0); i < height; ++i)
		for(size_t j(0); j < width; ++j)
		{
			const auto pos(matrix * math::vec<float>(2, {float(j), float(i)}));
			const auto off(ENTRY_OFFSET(pos[0], pos[1]));
			if(off < tmp.size())
				tmp[off] = *get(j, i);
		}
	std::copy(tmp.begin(), tmp.end(), data.begin());
}

void image::transform(const math::mat<float> &matrix, const math::vec<float> &origin)
{
	std::vector<float> tmp(BUFFER_SIZE(width, height, channels));

	for(size_t i(0); i < height; ++i)
		for(size_t j(0); j < width; ++j)
		{
			const auto pos(matrix * (math::vec<float>(2, {float(j), float(i)}) - origin) + origin);
			const auto off(ENTRY_OFFSET(pos[0], pos[1]));
			if(off < tmp.size())
				tmp[off] = *get(j, i);
		}
	std::copy(tmp.begin(), tmp.end(), data.begin());
}

void image::apply_kernel(const math::mat<float> &kernel)
{
	// TODO
	(void) kernel;
}

void image::crop()
{
	// TODO
}

void image::expand()
{
	// TODO
}

float *image::get(size_t x, size_t y)
{
	return &data[ENTRY_OFFSET(x, y)];
}

const float *image::get(size_t x, size_t y) const
{
	return &data[ENTRY_OFFSET(x, y)];
}

dataset<float> image::to_dataset()
{
	// TODO
	return dataset<float>(channels);
}

static image_format get_image_format(const std::string &buffer)
{
	if(buffer.size() > 2 && buffer[0] == 'B' && buffer[1] == 'M')
		return BMP;
	else if(buffer.find("\x89\x50\x4e\x47\x0d\x0a\x1a\x0a") == 0)
		return PNG;
	// TODO JPEG
	return RAW;
}

image image::read(const std::string &file)
{
	const auto buff(read_file(file));
	const auto format(get_image_format(buff));

	// TODO
	(void) format;
	return image(0, 0, 0);
}

void image::write(const std::string &file, image_format format)
{
	// TODO
	(void) file;
	(void) format;
}
