#ifndef SALMON_HPP
# define SALMON_HPP

# include <tuple>
# include <string>
# include <vector>

# include <linear_algebra.hpp>

namespace salmon
{
	template<typename T>
	class dataset
	{
		public:
		inline dataset(size_t features_count)
			: features(features_count)
		{}

		inline size_t features_count() const
		{
			return features.size();
		}

		void push(const T &x, const T &y)
		{
			size_t size, i;

			if(features.size() != 1)
				throw std::invalid_argument("Values count doesn't correspond to features count!");
			features.front().push_back(x);
			regressand.push_back(y);
			++samples_count;
		}

		void push(const math::vec<T> &x, const T &y)
		{
			size_t size, i;

			if(features.size() != x.size())
				throw std::invalid_argument("Values count doesn't correspond to features count!");
			for(i = 0; i < features.size(); ++i)
				features[i].push_back(x[i]);
			regressand.push_back(y);
			++samples_count;
		}

		inline void clear()
		{
			features.clear();
			samples_count = 0;
		}

		math::mat<T> regressor_matrix() const
		{
			math::mat<T> m(samples_count + 1, features.size());

			for(size_t i(1); i < samples_count; ++i)
			{
				m(i, 0) = 1;
				for(size_t j(0); j < features.size(); ++j)
					m(i, j) = features[j][i - 1];
			}
			return m;
		}

		math::mat<T> vandermonde_regressor_matrix(size_t feature, size_t degree)
		{
			math::mat<T> m(degree + 1);
			const auto &data(features[feature]);

			for(size_t i(0); i < data.size(); ++i)
			{
				T x(1);
				for(size_t j(0); j <= degree; ++j)
				{
					m[i][j] = x;
					x *= data[i];
				}
			}
			return m;
		}

		inline math::vec<T> get_regressand() const
		{
			return regressand;
		}

		private:
		std::vector<std::vector<T>> features;
		std::vector<T> regressand;

		size_t samples_count = 0;
	};
}

#endif
