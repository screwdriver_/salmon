#include <regression/regression.hpp>

using namespace salmon;

math::vec<float> salmon::ordinary_least_square(const math::mat<float> &regressor_matrix,
	const math::vec<float> &regressand)
{
	const auto &transpose_matrix(regressor_matrix.transpose_matrix());
	std::cout << transpose_matrix.height() << ", " << transpose_matrix.width() << '\n';
	std::cout << regressand.size() << '\n';
	return (transpose_matrix * regressor_matrix).inverse_matrix() * (transpose_matrix * regressand);
}
