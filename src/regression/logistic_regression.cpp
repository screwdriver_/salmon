#include <regression/regression.hpp>

using namespace salmon;

float salmon::logistic_function(const math::vec<float> &beta, const math::vec<float> &x)
{
	// TODO Assert that x.size() == beta.size()
	// TODO Assert that beta is not empty
	const auto n((beta.transpose_matrix() * x)[0]);
	return float(1) / (float(1) + exp(-n));
}

math::vec<float> salmon::logistic_regression(const dataset<float> &d)
{
	const logistic_cost_function<float> cost_func;
	math::vec<float> v(d.features_count());
	// TODO Change constants
	gradient_descent<float>(v, 0.1, 0.001, 1024, [&d, &cost_func](const size_t n, const float &x)
		{
			return cost_func.derivative(d, n, x);
		});
	return v;
}
