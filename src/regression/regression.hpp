#ifndef REGRESSION_HPP
# define REGRESSION_HPP

# include <salmon.hpp>
# include <util/cost_func.hpp>

# include <math.hpp>

namespace salmon
{
	inline float linear_function(const math::vec<float> &beta, const math::vec<float> &x)
	{
		return (beta.transpose_matrix() * x)(0, 0);
	}

	float polynomial_function(const math::vec<float> &beta, float x);
	float logistic_function(const math::vec<float> &beta, const math::vec<float> &x);

	math::vec<float> ordinary_least_square(const math::mat<float> &regressor_matrix,
		const math::vec<float> &regressand);
	math::vec<float> logistic_regression(const dataset<float> &d);

	template<typename T>
	void gradient_descent(math::vec<T> &v, const float gamma, const float error, const size_t max_iter,
		const std::function<T(size_t, const T &)> &derivative)
	{
		size_t i(0), n;

		while(i < max_iter)
		{
			const auto tmp(v);
			for(n = 0; n < v.size(); ++n)
				v[n] -= derivative(n, v[n]) * gamma;
			if((v - tmp).to_vec().length() < error)
				break;
			++i;
		}
	}
}

#endif
