#include <regression/regression.hpp>

using namespace salmon;

math::vec<float> salmon::ordinary_least_square(const math::mat<float> &regressor_matrix, const math::vec<float> &values)
{
	const auto &transpose_matrix(regressor_matrix.transpose_matrix());
	return (transpose_matrix * regressor_matrix).inverse_matrix() * (transpose_matrix * values);
}
