#include <regression/regression.hpp>

using namespace salmon;

float salmon::polynomial_function(const math::vec<float> &beta, float x)
{
	float v(0);
	float x_(1);

	for(size_t i(0); i < beta.size(); ++i)
	{
		v = std::fma(beta[i], x_, v);
		x_ *= x;
	}
	return v;
}
