#ifndef UTIL_HPP
# define UTIL_HPP

# include <string>

namespace salmon
{
	std::string read_file(const std::string &file);
	void write_file(const std::string &file, const std::string &data);
}

#endif
