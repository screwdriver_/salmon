#ifndef COST_FUNC_HPP
# define COST_FUNC_HPP

# include <cmath>
# include <functional>
# include <vector>

# include <salmon.hpp>

namespace salmon
{
	template<typename T>
	struct cost_function
	{
		virtual T function(const math::vec<T> &x, const math::vec<T> &y, const math::vec<T> &beta) const = 0;
		virtual T derivative(const dataset<T> &d, const size_t j, const T &beta) const = 0;
	};

	template<typename T>
	struct logistic_cost_function : public cost_function<T>
	{
		T function(const math::vec<T> &x, const math::vec<T> &y, const math::vec<T> &beta) const override
		{
			T sum(0);
			size_t i;
			const auto samples_count(x.size());

			for(i = 0; i < samples_count; ++i)
			{
				const auto h(1. / (1. + exp(-(x * beta)[0])));
				sum += y[i] * log(h) + (1. + y[i]) * log(1. - h);
			}
			return -sum / samples_count;
		}

		T derivative(const dataset<T> &d, const size_t j, const T &beta) const override
		{
			T sum(0);
			const auto &regressor_matrix(d.regressor_matrix());
			const auto samples_count(regressor_matrix.height());
			const auto &regressand(d.get_regressand());

			for(size_t i(0); i < samples_count; ++i)
			{
				const auto x(regressor_matrix(i, j));
				const auto h(1. / (1. + exp(-(x * beta))));
				sum += (h - regressand[i]) * x;
			}
			return sum / samples_count;
		}
	};
}

#endif
