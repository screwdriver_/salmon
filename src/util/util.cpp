#include <util/util.hpp>

#include <fstream>
#include <iostream>

using namespace salmon;

std::string salmon::read_file(const std::string &file)
{
	std::ifstream stream(file);
	size_t size;

	stream.seekg(0, std::ios::end);
	size = stream.tellg();
	stream.seekg(0, std::ios::beg);

	std::string buffer;
	buffer.resize(size);
	stream.read(&buffer[0], size);
	return buffer;
}

void salmon::write_file(const std::string &file, const std::string &data)
{
	std::ofstream stream(file);
	stream << data;
}
