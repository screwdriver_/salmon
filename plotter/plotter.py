import matplotlib.pyplot as plt

def show_plot(points_x, points_y, frange, f):
	plt.scatter(points_x, points_y, color="blue")
	plt.plot(frange, f, color="red")
	plt.xlabel("x")
	plt.ylabel("y")
	plt.title("Function plot")
	plt.grid(True, alpha=0.4)
	plt.show()
	return
