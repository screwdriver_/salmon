import sys
import numpy as np
import plotter
import math

curve_step = 0.1

def print_usage():
	print("usage: plotter <linear (<alpha> <beta> [samples...])|polynomial <constants...> [-- samples...]>|logistic <constants...> [-- samples...]")
	exit(1)

def get_range(points_x):
	if len(points_x) <= 0:
		begin = -10
		end = 10
	elif len(points_x) <= 1:
		begin = points_x[0] - 10
		end = points_x[0] + 10
	else:
		begin = min(float(i) for i in points_x) * 1.25
		end = max(float(i) for i in points_x) * 1.25
	return np.arange(begin, end, curve_step)

def handle_linear(argv):
	if len(argv) <= 3 or (len(argv) - 4) % 2 != 0:
		print_usage()
	points_x = [float(i) for i in argv[4::2]]
	points_y = [float(i) for i in argv[5::2]]
	alpha = float(argv[2])
	beta = float(argv[3])
	frange = get_range(points_x)
	plotter.show_plot(points_x, points_y, frange, alpha + beta * frange)
	return 0

# TODO Clean
def parse_constants(argv):
        if len(argv) <= 3:
                print_usage()
        argv = argv[2::]

        try:
                constants_end = argv.index("--")
        except ValueError:
                constants_end = len(argv)
        if constants_end == 0:
                constants_end = len(argv)

        constants = [float(i) for i in argv[:constants_end:]]
        if constants_end < len(argv):
                if (len(argv) - (constants_end + 1)) % 2 != 0:
                    print_usage()
                points_x = [float(i) for i in argv[constants_end + 1::2]]
                points_y = [float(i) for i in argv[constants_end + 2::2]]
        else:
                points_x = []
                points_y = []
        return (constants, points_x, points_y)

def handle_polynomial(argv):
	values = parse_constants(argv)
	constants = values[0]
	points_x = values[1]
	points_y = values[2]
	frange = get_range(points_x)

	curve = 0
	for i in range(0, len(constants)):
		curve += constants[i] * frange**i

	plotter.show_plot(points_x, points_y, frange, curve)
	return 0

def handle_logistic(argv):
	values = parse_constants(argv)
	constants = values[0]
	points_x = values[1]
	points_y = values[2]
	frange = get_range(points_x)

	curve = 0
	for i in range(0, len(constants)):
		if i % 2 == 0:
			curve += constants[i]
		else:
			curve += constants[i] * frange # TODO Handle more than one axis
	curve = 1 / (1 + math.e**-curve)

	plotter.show_plot(points_x, points_y, frange, curve)
	return 0

def main(argv):
	if len(argv) <= 1:
		print_usage()
	
	modes = [("linear", handle_linear), ("polynomial", handle_polynomial), ("logistic", handle_logistic)]
	for m in modes:
		if argv[1] == m[0]:
			exit(m[1](argv))
	print_usage()

main(sys.argv)
